﻿<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ page import="java.sql.*, javax.sql.*,java.io.*,java.net.*" %>
<%@ page import="javax.xml.parsers.*, org.w3c.dom.*" %>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<h1>wifi 조회</h1>
<%
	// 파싱을 위한 준비과정
	DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	
	// 파일을 읽을때 서버내부 local path(전체경로)로 지정 
	// .. 이 문장이 xml 파싱을 한다. (위 xml 관련 임포트 주의)
	Document doc = docBuilder.parse(new File("/var/lib/tomcat8/webapps/ROOT/biz02_ch02/wifi.xml"));
	
	Element root = doc.getDocumentElement();				// root태그 (여기서 사용 X)
	NodeList tag_addr = doc.getElementsByTagName("소재지지번주소");	// xml 소재지지번주소 tag
	NodeList tag_addr2 = doc.getElementsByTagName("소재지도로명주소");	// xml 소재지도로명주소 tag
	NodeList tag_latitude = doc.getElementsByTagName("위도");		// xml 위도 tag
	NodeList tag_longitude = doc.getElementsByTagName("경도");		// xml 경도 tag		
%>

<table border=1 style="width: 800px; margin: 10px; border: solid 1px #000000; text-align: center;">
	<thead style="background-color: #dbdbdb; color: #000000;">
	<tr>
		<th width="10%">순번</th>
		<th width="50%">소재지 지번주소</th>
		<th width="20%">위도</th>
		<th width="20%">경도</th>		
	</tr>
	</thead>
	<tbody>
<%
	for(int i = 0; i < tag_addr.getLength(); i++) {
		
		// 소재지지번주소가 null 인 경우 소재지도로명주소로 대체하여 출력
		try{
			out.println("<tr>");
			out.println("<td>"+(i+1)+"</td>");	// 순번 출력
			out.println("<td>"+tag_addr.item(i).getFirstChild().getNodeValue()+"</td>");	// 소재지지번주소 출력
		} catch(Exception e){
			out.println("<td>"+tag_addr2.item(i).getFirstChild().getNodeValue()+"</td>");	// 소재지도로명주소 출력
		}
%>	
		
		<td><%=tag_latitude.item(i).getFirstChild().getNodeValue()%></td>	<!-- 위도 출력 -->
		<td><%=tag_longitude.item(i).getFirstChild().getNodeValue()%></td>	<!-- 경도 출력 -->	
	</tr>
<%	
	}
%>
	</tbody>
</table>

</body>
</html>